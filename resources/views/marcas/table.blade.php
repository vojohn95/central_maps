<div class="table-responsive ">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center ">
        <thead>
        <tr>
            <th>Marca</th>
            <th>Acción</th>

        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>

        <tbody>
        @foreach($marcas as $marca)
            <tr>
                <td>{!! $marca->marca !!}</td>
                <td>
                    {!! Form::open(['route' => ['marcas.destroy', $marca->id], 'method' => 'delete']) !!}

                        <a href="{!! route('marcas.edit', [$marca->id]) !!}" class='btn btn-default btn-xs white-text'><i
                                    class="glyphicon glyphicon-edit"></i>EDITAR</a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>ELIMINAR', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


