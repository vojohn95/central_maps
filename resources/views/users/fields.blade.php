<!-- Name Field -->
<div class="md-form">
    <i class="fas fa-user prefix"></i>
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="md-form">
    <i class="fas fa-envelope prefix"></i>
    {!! Form::label('email', 'Correo:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>


<!-- Password Field -->
<div class="md-form">
    <i class="fas fa-unlock-alt prefix"></i>
    {!! Form::label('password', 'Contraseña:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Permiso Field-->
<div class="md-form">
    <i class="fas fa-unlock-alt prefix"></i>
    {!! Form::label('password', 'Confirmar Contraseña:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!--Blue select-->
<select class="mdb-select md-form colorful-select dropdown-success" name="permiso">
    <option value="" selected disabled>Seleccione un perfil</option>
    @forelse($permiso as $item)
        <option value="{{$item->id}}">{{$item->nombre}}</option>
    @empty
        <option value="">Sin permisos</option>
    @endforelse
</select>

<!--/Blue select-->

<!-- Submit Field -->
<div class="md-form">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancelar</a>
</div>

@push('scripts')
    $(document).ready(function() {
    $('.mdb-select').materialSelect();
    });
@endpush
