<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Telefono</th>
            <th>Empresa</th>
            <th>Estado</th>
            <th>Acción</th>

        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>

        <tbody>
        @foreach($gerentes as $gerente)
            <tr>
                <td>{!! $gerente->nombre !!}</td>
                <td>{!! $gerente->email !!}</td>
                <td>{!! $gerente->telefono !!}</td>
                <td>{!! $gerente->empresa !!}</td>
                <td>{!! $gerente->estado !!}</td>
                {!! Form::open(['route' => ['gerentes.destroy', $gerente->id], 'method' => 'delete']) !!}
                <td>
                    <a href="{!! route('gerentes.edit', [$gerente->id]) !!}" class='btn btn-default btn-xs white-text'><i
                                class="glyphicon glyphicon-edit"></i>Editar</a>

                    {!! Form::button('<i class="glyphicon glyphicon-trash"><span>Eliminar</span></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
