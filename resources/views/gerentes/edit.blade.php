@extends('layouts.app')
@section('title', 'Gerentes')

@section('content')

    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Editar gerente</h3>
        </div>
    </div>
    <hr>

    <div class="row justify-content-md-center">
        <div class="col-md-8">
            @include('layouts.errors')

            {!! Form::model($gerente, ['route' => ['gerentes.update', $gerente->id], 'method' => 'patch']) !!}

            @include('gerentes.fields')
            {!! Form::close() !!}
        </div>
    </div>

@endsection
