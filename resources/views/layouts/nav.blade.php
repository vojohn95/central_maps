<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">
    <div class="container smooth-scroll">
        <a class="navbar-brand " href="{{url('/')}}"><img src="{{asset('logo/logo/login-logo.png')}}"
                                                          style="width: 40px;">entral maps</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}"><span class="btn btn-default btn-rounded btn-sm"><i
                                    class="fas fa-home"></i>INICIO</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/login') }}"><span class="btn btn-default btn-rounded btn-sm"><i
                                    class="fas fa-user" aria-hidden="true"></i>Ingresar</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
