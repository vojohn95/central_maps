<!-- Id Vacante Field -->
<div class="md-form">
    {!! Form::hidden('id_vacante', $unico , ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="md-form">
    <i class="fas fa-address-card prefix"></i>
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Correo Field -->
<div class="md-form">
    <i class="fas fa-envelope prefix"></i>
    {!! Form::label('correo', 'Correo:') !!}
    {!! Form::text('correo', null, ['class' => 'form-control']) !!}
</div>

<!-- Tel Field -->
<div class="md-form">
    <i class="fas fa-phone prefix"></i>
    {!! Form::label('tel', 'Telefono:') !!}
    {!! Form::text('tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Field -->
<div class="md-form">
    <i class="fas fa-directions prefix"></i>
    {!! Form::label('direccion', 'Dirección:') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Pais Field -->
<div class="md-form">
    <i class="fas fa-globe prefix"></i>
    {!! Form::label('pais', 'País:') !!}
    {!! Form::text('pais', null, ['class' => 'form-control']) !!}
</div>

<!-- Edo Prov Field -->
<div class="md-form">
    <i class="fas fa-globe-americas prefix"></i>
    {!! Form::label('edo_prov', 'Estado/Provincia:') !!}
    {!! Form::text('edo_prov', null, ['class' => 'form-control']) !!}
</div>

<!-- Mun Cd Field -->
<div class="md-form">
    <i class="fas fa-compass prefix"></i>
    {!! Form::label('mun_cd', 'Municipio/Delegación:') !!}
    {!! Form::text('mun_cd', null, ['class' => 'form-control']) !!}
</div>

<!-- Colonia Field -->
<div class="md-form">
    <i class="fas fa-map-signs prefix"></i>
    {!! Form::label('colonia', 'Colonia:') !!}
    {!! Form::text('colonia', null, ['class' => 'form-control']) !!}
</div>

<!-- Calles Field -->
<div class="md-form">
    <i class="fas fa-road prefix"></i>
    {!! Form::label('calles', 'Calles:') !!}
    {!! Form::text('calles', null, ['class' => 'form-control']) !!}
</div>

<!-- Numeroext Field -->
<div class="md-form">
    <i class="fas fa-map-marker prefix"></i>
    {!! Form::label('numeroExt', 'Numeroext:') !!}
    {!! Form::text('numeroExt', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="md-form">
    <i class="fas fa-map-marker-alt prefix"></i>
    {!! Form::label('cp', 'Codigo postal:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Field -->
<div class="md-form">
    {!! Form::hidden('estado', 'postulado' , ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="md-form">
    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
    <a href="{{route('rutas', $unico)}}" class="btn btn-default float-lg-right">Cancelar</a>
</div>
