<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $postulante->id !!}</p>
</div>

<!-- Id Vacante Field -->
<div class="form-group">
    {!! Form::label('id_vacante', 'Id Vacante:') !!}
    <p>{!! $postulante->id_vacante !!}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $postulante->nombre !!}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{!! $postulante->correo !!}</p>
</div>

<!-- Tel Field -->
<div class="form-group">
    {!! Form::label('tel', 'Tel:') !!}
    <p>{!! $postulante->tel !!}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{!! $postulante->direccion !!}</p>
</div>

<!-- Pais Field -->
<div class="form-group">
    {!! Form::label('pais', 'Pais:') !!}
    <p>{!! $postulante->pais !!}</p>
</div>

<!-- Edo Prov Field -->
<div class="form-group">
    {!! Form::label('edo_prov', 'Edo Prov:') !!}
    <p>{!! $postulante->edo_prov !!}</p>
</div>

<!-- Mun Cd Field -->
<div class="form-group">
    {!! Form::label('mun_cd', 'Mun Cd:') !!}
    <p>{!! $postulante->mun_cd !!}</p>
</div>

<!-- Colonia Field -->
<div class="form-group">
    {!! Form::label('colonia', 'Colonia:') !!}
    <p>{!! $postulante->colonia !!}</p>
</div>

<!-- Calles Field -->
<div class="form-group">
    {!! Form::label('calles', 'Calles:') !!}
    <p>{!! $postulante->calles !!}</p>
</div>

<!-- Numeroext Field -->
<div class="form-group">
    {!! Form::label('numeroExt', 'Numeroext:') !!}
    <p>{!! $postulante->numeroExt !!}</p>
</div>

<!-- Cp Field -->
<div class="form-group">
    {!! Form::label('cp', 'Cp:') !!}
    <p>{!! $postulante->cp !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $postulante->estado !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $postulante->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $postulante->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $postulante->deleted_at !!}</p>
</div>

