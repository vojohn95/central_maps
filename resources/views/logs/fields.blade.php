<!-- Id User Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_user', 'Id User:') !!}
    {!! Form::number('id_user', null, ['class' => 'form-control']) !!}
</div>

<!-- Mensaje Sis Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mensaje_sis', 'Mensaje Sis:') !!}
    {!! Form::text('mensaje_sis', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Metodo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('metodo', 'Metodo:') !!}
    {!! Form::text('metodo', null, ['class' => 'form-control']) !!}
</div>

<!-- Tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo', 'Tipo:') !!}
    {!! Form::text('tipo', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('logs.index') !!}" class="btn btn-default">Cancel</a>
</div>
