@extends('layouts.app')

@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Vacante</h3>
        </div>
    </div>
    <hr>
    @include('flash::message')
    @include('layouts.errors')
    {!! Form::model($vacante, ['route' => ['vacantes.update', $vacante->id], 'method' => 'patch']) !!}
    <div class="row justify-content-md-center">
        <div class="col-md-8">
            @include('vacantes.fields')
        </div>
    </div>
    {!! Form::close() !!}

    <br>
@endsection
