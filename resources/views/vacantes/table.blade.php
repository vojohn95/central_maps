<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Estacionamiento</th>
            <th>Puesto</th>
            <th>Área</th>
            <th>Horario</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Action</th>

        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>

        <tbody>
        @foreach($vacantes as $item)
            <tr>
                <td>{{$item->estacionamiento}}</td>
                <td>{{$item->puesto}}</td>
                <td>{{$item->area}}</td>
                <th>{{$item->horario}}</th>
                <td>{{$item->fecha}}</td>
                <td>

                    @switch($item->estado)
                        @case('Activa')
                        <i class="fas fa-thumbs-up fa-2x material-tooltip-main" data-toggle="tooltip"
                           data-placement="bottom" title="Activa"></i>
                        @break
                        @case('Proceso')
                        <i class="fas fa-exchange-alt fa-2x material-tooltip-main" data-toggle="tooltip"
                           data-placement="bottom" title="Proceso"></i>
                        @break
                        @case('Finalizada')
                        <i class="fas fa-times fa-2x material-tooltip-main" data-toggle="tooltip"
                           data-placement="bottom" title="Finalizada"></i>
                        @break
                    @endswitch
                </td>
                {!! Form::open(['route' => ['vacantes.destroy', $item->id], 'method' => 'delete']) !!}
                <td>
                    <a href="{!! route('vacantes.edit', [$item->id]) !!}" class='btn btn-default btn-xs white-text'><i
                            class="glyphicon glyphicon-edit"></i>Visualizar</a>

                    <a href="{!! route('vacantes.postulados', [$item->id]) !!}"
                       class='btn btn-default btn-xs white-text'><i
                            class="glyphicon glyphicon-edit"></i>Postulados</a>

                    {!! Form::button('<i class="glyphicon glyphicon-trash"><span>Eliminar</span></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
