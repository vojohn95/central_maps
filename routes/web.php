<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PersonasController@index');
Route::post('/puesto', 'PersonasController@AjaxRequestPuestos')->name('Puestos');
Route::post('/estacionamiento', 'PersonasController@AjaxRequestEstacionamiento')->name('Estacionamiento');
Route::post('/marca', 'PersonasController@AjaxRequestMarcas')->name('marca');
Route::post('/pais', 'PersonasController@AjaxRequestPais')->name('Pais');
Route::post('/estado', 'PersonasController@AjaxRequestEstado')->name('Estado');
Route::post('/municipio', 'PersonasController@AjaxRequestMunicipio')->name('Municipio');
Route::get('/detalle_ruta/{id}', 'PersonasController@rutas')->name('rutas');
Route::get('/central_maps/{id}', 'PostulanteController@create')->name('post');
Route::resource('postulantes', 'PostulanteController');
Route::get('/vacantes/{id}/postulados', 'VacanteController@postulados')->name('vacantes.postulados');
Route::post('/vacantes/{id}/postulados', 'VacanteController@actualizaPostulado')->name('Actualiza');

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::resource('users', 'UserController');

    Route::resource('vacantes', 'VacanteController');

    Route::resource('rols', 'RolController');

    Route::resource('puestos', 'PuestoController');

    Route::resource('permisos', 'PermisoController');

    Route::resource('logs', 'LogController');

    Route::resource('gerentes', 'GerenteController');

    Route::resource('marcas', 'MarcaController');

    Route::resource('proyectos', 'ProyectoController');
});
