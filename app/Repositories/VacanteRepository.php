<?php

namespace App\Repositories;

use App\Models\Vacante;
use App\Repositories\BaseRepository;

/**
 * Class VacanteRepository
 * @package App\Repositories
 * @version August 22, 2019, 2:59 pm UTC
 */
class VacanteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_proyecto',
        'id_puesto',
        'id_user',
        'horario',
        'salario',
        'fecha',
        'tel',
        'email',
        'estado'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Vacante::class;
    }
}
