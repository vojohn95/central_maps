<?php

namespace App\Repositories;

use App\Models\Puesto;
use App\Repositories\BaseRepository;

/**
 * Class PuestoRepository
 * @package App\Repositories
 * @version August 22, 2019, 3:01 pm UTC
 */
class PuestoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'puesto',
        'area',
        'req',
        'des'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Puesto::class;
    }
}
