<?php

namespace App\Repositories;

use App\Models\Postulante;
use App\Repositories\BaseRepository;

/**
 * Class PostulanteRepository
 * @package App\Repositories
 * @version August 22, 2019, 3:03 pm UTC
 */
class PostulanteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_vacante',
        'nombre',
        'correo',
        'tel',
        'direccion',
        'pais',
        'edo_prov',
        'mun_cd',
        'colonia',
        'calles',
        'numeroExt',
        'cp',
        'estado'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Postulante::class;
    }
}
