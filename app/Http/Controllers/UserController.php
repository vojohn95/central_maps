<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Permiso;
use App\Models\Rol;
use App\Models\User;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $users = DB::table('users')
            ->join('roles', 'users.id', '=', 'roles.id_user')
            ->join('permisos', 'roles.id_permiso', '=', 'permisos.id')
            ->select('users.id', 'users.name', 'users.email', 'permisos.nombre')
            ->get();
        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $permisos = Permiso::all();
        return view('users.create')
            ->with('permiso', $permisos);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store()
    {
        $input = request()->validate([
            'permiso' => ['required'],
            'password' => ['required'],
            'email' => ['required'],
            'name' => ['required'],
        ]);
        $input['password'] = Hash::make($input['password']);
        $user = $this->userRepository->create($input);
        $rol = new Rol;
        $rol->id_user = $user->id;
        $rol->id_permiso = $input['permiso'];
        $rol->save();
        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = DB::table('users')
            ->join('roles', 'users.id', '=', 'roles.id_user')
            ->join('permisos', 'roles.id_permiso', '=', 'permisos.id')
            ->where('users.id', '=', $id)
            ->select('users.*', 'permisos.nombre')
            ->get();
        if (empty($user[0])) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }
        $permisos = Permiso::all();
        return view('users.edit')
            ->with('user', $user[0])
            ->with('permiso', $permisos);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id)
    {
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }
        $input = \request()->validate([
            'permiso' => ['required'],
            'password' => [''],
            'email' => ['required'],
            'name' => ['required'],
        ]);
        if ($input['password'] == null) {
            $user2 = User::find($id);
            $user2->name = $input['name'];
            $user2->email = $input['email'];
            $user2->save();
            $rol = DB::table('roles')
                ->where('id_user', '=', $id)
                ->get();
            $roles = Rol::find($rol[0]->id);
            $roles->id_permiso = $input['permiso'];
            $roles->save();
        } else {
            $input['password'] = Hash::make($input['password']);
            $user = $this->userRepository->update($input, $id);
            $rol = DB::table('roles')
                ->where('id_user', '=', $id)
                ->get();
            $roles = Rol::find($rol[0]->id);
            $roles->id_permiso = $input['permiso'];
            $roles->save();
        }

        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}
