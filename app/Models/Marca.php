<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Marca
 * @package App\Models
 * @version August 22, 2019, 4:52 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection proyectos
 * @property \Illuminate\Database\Eloquent\Collection
 * @property string marca
 */
class Marca extends Model
{

    public $table = 'marcas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'marca'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'marca' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'marca' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function proyectos()
    {
        return $this->hasMany(\App\Models\Proyecto::class);
    }
}
